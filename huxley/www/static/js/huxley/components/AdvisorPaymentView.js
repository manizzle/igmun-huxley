
'use strict';

var React = require('react');
var Router = require('react-router');

var NavTab = require('./NavTab');
var PermissionDeniedView = require('./PermissionDeniedView');
var TopBar = require('./TopBar');
var InnerView = require('./InnerView');
var CurrentUserStore = require('../stores/CurrentUserStore');

var AdvisorPaymentView = React.createClass ({
    mixins: [
        React.addons.LinkedStateMixin,
    ],
    render: function() {
        var user = CurrentUserStore.getCurrentUser();
        var school = user.getSchool();
        return (<InnerView>
                <p>If you would like to pay
                now the options are to send check and/or cash to
                4562 Mayfield Court, Fremont, CA.</p>
                <p>
                You can also pay via Paypal. The paid sum
                will be marked on your Profile page in a few days.
                </p>
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_xclick"/>
                <input type="hidden" name="business" value="ZY588NJL22DZ2"/>
                <input type="hidden" name="lc" value="US"/>
                <input type="hidden" name="item_name" value="IGMUN Registration Ticket"/>
                <input type="hidden" name="item_number" value="1337"/>
                <input type="hidden" name="amount" value={school.fees_owed}/>
                <input type="hidden" name="currency_code" value="USD"/>
                <input type="hidden" name="button_subtype" value="services"/>
                <input type="hidden" name="no_note" value="0"/>
                <input type="hidden" name="cn" value="Add special instructions to the seller:"/>
                <input type="hidden" name="no_shipping" value="2"/>
                <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHosted"/>
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"/>
                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"/>
                </form>
                </InnerView>);
    }
});
  
module.exports = AdvisorPaymentView;
